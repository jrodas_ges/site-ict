<?php
/*
    Template Name: Page-template
    @todo cambiar la imagen del banner por imagen destacada y cargar la imagen por defecto en el caso de que no se encuentre
 */
wp_enqueue_style( 'index',  get_template_directory_uri() . '/index.css' );
get_header();
$path = 'default';
if(is_page('por-que-guatemala')){
    $path = 'por-que-guatemala';
}

if(have_posts()) {
    while (have_posts()) {
        the_post();
        $parent = get_post_ancestors(get_the_ID());
        $postParentID = ($parent[0] != 0) ? $parent[0] : get_the_ID();
        $postParent = get_post($postParentID);

        if (has_post_thumbnail($postParent->ID) ) {
            $banner=get_the_post_thumbnail($postParent->ID);
        }else{
            $banner='<img src="'. get_bloginfo('template_url').'/img/default/banner.jpg " alt="'. get_the_title() .'"/> ';
        }

    }
}
?>
    <div id="index">
        <div>
            <div id="banner-index">
                <?php
                    echo $banner;
                ?>
            </div>
        </div>

    </div>
<?php
if(have_posts()) {
    while (have_posts()) {
        the_post();
        $parent = get_post_ancestors(get_the_ID());
        $postParentID = ($parent[0]!=0)?$parent[0]:get_the_ID();
        $postParent = get_post($postParentID);
        ?>
        <div  id="page-container">
        <div id="page-content">
        <div id="title-container" class="background-claro">
            <?php
                $url_icon  = get_bloginfo('template_url') . '/img/' . $postParent->post_name . '/icono.png';
                $path_icon = get_template_directory() . '/img/' . $postParent->post_name . '/icono.png';
                if (!is_readable($path_icon)){
                    $url_icon  = get_bloginfo('template_url') . '/img/default/icono.png';
                }
            ?>
            <div class="icon-title">
                <img  class="icono-<?php echo $postParent->post_name; ?>" src="<?php echo $url_icon ?>" alt="<?php the_title() ?>"/>
            </div>
            <div class="title-page">
                <h1 style="font-size: 24px;margin-left: 10px;">
                    <?php  echo get_the_title($postParent->ID); ?>
                </h1>
            </div>
        </div>
        <div id="content-page">
        <div class="sidebar">
            <ul class="list-actors sidebar-page-actors">
                <li>
                    <a  style="color:#0F79A0" title="<?php the_title();?>" href="<?php echo get_the_permalink($postParent); ?>">
                        <?php
                            echo get_the_title($postParent->ID);
                        ?>
                    </a>
                </li>
                <?php
                $args=array(
                    'post_type' => 'page',
                    'order'     => 'ASC',
                    'order_by'  => 'menu_order',
                    'post_parent' => $postParent->ID
                );

                $news = new WP_Query($args);
                if($news->have_posts()) {
                    while ($news->have_posts()) {
                        $news->the_post();
                        ?>
                        <li>
                            <a  style="color:#0F79A0" title="<?php the_title();?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>
                    <?php
                    }
                }
                wp_reset_query();
                ?>
            </ul>
        </div>
        <div class="content-page-actors">
        <?php
            the_title('<h1>', '</h1>');
            the_content();
        }
    }
?>
    </div>
    <div class="clear"></div>
    </div>
    </div>
    </div>
<?php
get_footer();
?>