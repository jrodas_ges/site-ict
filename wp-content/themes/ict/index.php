<?php
    wp_enqueue_style( 'index',  get_template_directory_uri() . '/index.css' );
    get_header();
?>
    <div id="index" style="background: #666">
        <div>
            <div id="banner-index">
                <img src="<?php bloginfo('template_url') ?>/img/default/banner.jpg" alt="Banner Proyecto ICT"/>
            </div>
        </div>
    </div>
    <div  id="page-container">
        <div id="index-content">
            <?php
                if(have_posts()) {
                    while (have_posts()){
                        the_post();
                        ?>
                        <div id="title-container" class="background-claro">
                            <div class="icon-title">
                            </div>
                            <div class="title-page">
                                <h1 style="font-size: 24px;margin-left: 10px;">
                                    <?php the_title(); ?>
                                </h1>
                            </div>
                        </div>
                        <div id="page-content">
                            <div class="index-conatainer">
                                <?php the_content()?>
                            </div>
                        </div>
                    <?php
                    }
                }
             ?>
        </div>
    </div>
<?php
    get_footer();
?>