<?php
/*
    Template Name: Página de Actor
 */
wp_enqueue_style( 'index',  get_template_directory_uri() . '/index.css' );
get_header();
?>
    <div id="index" style="background: #666">
        <div>
            <div id="banner-index">
                <img src="<?php bloginfo('template_url'); ?>/img//actores/banner.jpg " alt="<?php the_title() ?>"/>
            </div>
        </div>
    </div>
<?php
if(have_posts()) {
    while (have_posts()) {
        the_post();
        $parent = get_post_ancestors(get_the_ID());
        $postParent = ($parent[0]!=0)?$parent[0]:get_the_ID();

        ?>
    <div  id="page-container">
        <div id="page-content">

            <div id="content-page">
                <div class="sidebar">
                    <ul class="list-actors sidebar-page-actors">
                        <?php
                        $args=array(
                            'post_type' => 'actors',
                            'order'     => 'ASC',
                            'order_by'  => 'menu_order',
                            'post_parent' => $postParent
                        );

                        $news = new WP_Query($args);
                        if($news->have_posts()) {
                            while ($news->have_posts()) {
                                $news->the_post();
				$id=get_the_ID();
                                ?>
                                <li>
                                    <a  style="color:#0F79A0" title="<?php the_title();?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </li>
                            <?php
                            }
                        }
                        wp_reset_query();
                        ?>
                    </ul>
                </div>
                <div class="content-page-actors">
                    <h1>
                        <?php echo get_the_title($postParent);  ?>
                    </h1>
                    <?php
                            if($parent[0]!=0) {
                                the_title('<h3>', '</h3>');
                                the_content();
                            }else{
                                $acercaDe=get_posts(array(
                                    'name'        => 'acerca-de',
                                    'post_type'   => 'actors',
                                    'post_status' => 'publish',
                                    'numberposts' => 1,
				    'post_parent'=> $postParent   
                                ));
				$args =  array(
                                    'name'        => 'acerca-de',
                                    'post_type'   => 'actors',
                                    'post_status' => 'publish',
                                    'numberposts' => 1,
                                    'post_parent'=> $postParent
				);
				$acerca = new WP_Query($args);
				if($acerca->have_posts()){
					while($acerca->have_posts()){
						$acerca->the_post();	
                             ?>
                                <h3><?php the_title(); ?></h3>
                                <?php the_content(); ?>
                            <?php
					}
				}
				wp_reset_query();
                            }
                        }
                    }
                    ?>
                </div>
                <div class="clear"></div>

            </div>
        </div>
    </div>
<?php
get_footer();
?>
