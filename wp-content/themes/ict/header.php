<?php
    $local = get_locale();
    $lang = 'en_US';
?>
<!doctype html>
<html class="no-js" lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php wp_title( '-', true, 'right' ); echo wp_specialchars( get_bloginfo('name'), 1 ) ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <link href="<?php bloginfo('template_url');?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.7/slick.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.7/slick.min.js"></script>
    <?php
        wp_enqueue_style( 'main', get_stylesheet_uri() );
        wp_head();

    ?>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
    <!--[if IE 7]>
        <style>
            .icon-home{
                padding-top: 60px;
            }
            .titulo-convocatoria h3{
                padding-top: 15px;
            }
            .ver-mas{
                margin-top: 90px;
            }
            .title-page h1{
                padding-top: 15px;
            }
            .container-main-menu{
                padding-top: 15px;
            }
        </style>
    <![endif]-->
</head>
<body>
<div id="main-container">
    <div id="header">
        <div id="logo">
            <a href="<?php bloginfo('home'); ?>" title="<?php bloginfo('name')?>">
                <img src="<?php bloginfo('template_url')?>/img/home/logo.png" alt="ICT GUATEMALA"/>
            </a>
        </div>
        <div id="main-menu">
            <div class="container-main-menu">
                <div style="float: left">
                    <?php
                    wp_nav_menu( array( 'theme_location' => 'Default' ) );
                    ?>
                </div>
                <ul style="float: right">
                    <li class="idioma">
                        <?php pll_the_languages(array('dropdown'=>1)); ?>
                    <li>
                </ul>
            </div>
        </div>
    </div>