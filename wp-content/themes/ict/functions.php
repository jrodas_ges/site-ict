<?php
/*
 * En el caso de  que no este activado el plugin polilang
 */

/*
 * Registro de stings para traduciónes de menú
 */
pll_register_string("actores", "Actores","Menu");
pll_register_string("noticias", "noticias","Menu");
pll_register_string("descaras", "descargas","Menu");
pll_register_string("enlaces", "enlaces","Menu");
pll_register_string("boletin", "boletin","Menu");
pll_register_string("idealist", "idealist","Menu");
pll_register_string("por que Guatemala", "por que Guátemala","Menu");
pll_register_string("contáctenos", "contáctenos","Menu");
pll_register_string("acerca de", "acerca de","Menu");
pll_register_string("historias", "historias","Menu");
pll_register_string("convocatorias", "convocatorias","Menu");
pll_register_string("cifras", "cifras","Menu");
pll_register_string("codigo postal", "Código postal","Menu");
pll_register_string("Listado de Actores", "Listado de Actores","Theme");

//pll_register_string("", "","Menu");

pll_register_string("ver más", "Ver más","Theme");
pll_register_string("Error 404", "Lo sentimos, la página solicitada no se encuentra disponible en estos momentos","Theme");
pll_register_string("Calle", "Calle","theme");
pll_register_string("Dirección", "Dirección","Theme");
pll_register_string("zona", "zona","Theme");
pll_register_string('Siguiente &raquo;', 'Siguiente &raquo;','theme');
pll_register_string('&laquo; Anterior','&laquo; Anterior;','theme');

load_theme_textdomain( 'ICT', TEMPLATEPATH.'/lang' );
/*
*
* Campo Personalizado para  Imagen destacada 2 en Eventos, Historias de exito, Noticias
*/
add_action("add_meta_boxes", "img_mas_metabox");
function img_mas_metabox()
{
    add_meta_box(
        "udp_caja",
        "Fecha de Convocatoria",
        "print_metabox",
        "post",
        "side"
    );
}

$udp_meta = array(
    array(
        "lbl"	=>"Dia",
        "id"   	=>"dia-convocatoria",
        "type"  =>"select",
        "range" => 31
    ),
    array(
        "lbl"	=>"Mes",
        "id"   	=>"mes-convocatoria",
        "type"  =>"select",
        "range" => 12
    )

);

function print_metabox(){
    global $udp_meta, $post;
    wp_nonce_field("udp_metabox_nonce","udp_box_nonce");
    foreach($udp_meta as $field)
    {
        $meta = get_post_meta($post->ID, $field["id"], true);
        switch ($field["type"]){
            case 'text':
                break;
            case 'select':
                ?>
                <br/>
                <label for="<?php echo $field['id'] ?>"><?php echo $field["lbl"] ?></label><br>
                <select  id="<?php echo $field['id'] ?>" name="<?php echo $field['id'] ?>">
                    <?php
                        for($x=1;$x<=$field['range'];$x++){
                    ?>
                        <option value="<?php echo $x ?>" <?php if($meta==$x){echo "selected";} ?> ><?php echo $x ?></option>
                    <?php
                        }
                    ?>
                </select>
                <?php
                break;
        }
    }
}

add_action("save_post", "save_metabox");
function save_metabox($post_id){
    if(!isset($_POST["udp_box_nonce"]) || !wp_verify_nonce($_POST["udp_box_nonce"], "udp_metabox_nonce")) {
        return;
    }
    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
        return;
    }
    if(!current_user_can("edit_post")){
        return;
    }
    global $udp_meta;

    foreach($udp_meta as $field) {

        $udpPostData = $_POST[$field["id"]];

        update_post_meta(
            $post_id,
            $field["id"],
            $field["type"] === "text" ? esc_attr($udpPostData) : $udpPostData
        );
    }
}
/*
 |
 |
 | Postype Actores
 |
 |
 */

function actor_post_type() {

    $labels = array(
        'name'                => _x( 'Actores', 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( 'Actors', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( 'Actores', 'text_domain' ),
        'name_admin_bar'      => __( 'Actores', 'text_domain' ),
        'parent_item_colon'   => __( 'Actor padre:', 'text_domain' ),
        'all_items'           => __( 'Ver todos los Actores', 'text_domain' ),
        'add_new_item'        => __( 'Añadir Actor', 'text_domain' ),
        'add_new'             => __( 'Añadir Actor', 'text_domain' ),
        'new_item'            => __( 'Nuevo Actor', 'text_domain' ),
        'edit_item'           => __( 'Editar Actor', 'text_domain' ),
        'update_item'         => __( 'Actualizar Actor', 'text_domain' ),
        'view_item'           => __( 'Editar Actor', 'text_domain' ),
        'search_items'        => __( 'Buscar Actor', 'text_domain' ),
        'not_found'           => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
    );
    $args = array(
        'label'               => __( 'actors', 'text_domain' ),
        'description'         => __( 'Post Type Description', 'text_domain' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', 'page-attributes', 'post-formats', 'template'),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type( 'actors', $args );

}
add_action( 'init', 'actor_post_type', 0 );

add_filter('pll_get_post_types', 'my_pll_get_post_types');
function my_pll_get_post_types($types) {
    return array_merge($types, array('actors' => 'actors'));
}

add_theme_support( 'post-thumbnails', array( 'post','page','actors'));
add_image_size( 'actor',210, 75 );


/*
 |
 |
 |  Helpers
 |
 |
 */
/*Cortar Textos*/
function get_excerpt_content($data, $charlength = 140, $trim_str = '') {
    $excerpt = strip_shortcodes(apply_filters('get_the_excerpt', $data));
    $charlength++;
    if (mb_strlen($excerpt) > $charlength) {
        $ret = '';
        $subex = mb_substr($excerpt, 0, $charlength - 5);
        $exwords = explode(' ', $subex);
        $excut = - ( mb_strlen($exwords[count($exwords) - 1]) );
        if ($excut < 0) {
            $ret .= mb_substr($subex, 0, $excut);
        } else {
            $ret .= $subex;
        }
        return $ret . $trim_str;
    } else {
        return $excerpt;
    }
}


  function getCategoryLang($original, $traslate){
      global $local, $lang;
      $category = ($local == $lang) ? $traslate :$original;
      $category = get_category_by_slug($category);
      return  $category;
  }


/*
 |
 | Páginación
 |
 */
/***** Numbered Page Navigation (Pagination) Code.
Tested up to WordPress version 3.1.2 *****/

/* Function that Rounds To The Nearest Value.
   Needed for the pagenavi() function */
function round_num($num, $to_nearest) {
    /*Round fractions down (http://php.net/manual/en/function.floor.php)*/
    return floor($num/$to_nearest)*$to_nearest;
}

/* Function that performs a Boxed Style Numbered Pagination (also called Page Navigation).
   Function is largely based on Version 2.4 of the WP-PageNavi plugin */
function pagenavi($before = '', $after = '') {
    global $wpdb, $wp_query;
    $pagenavi_options = array();
    $pagenavi_options['pages_text'] = ('Página %CURRENT_PAGE% de %TOTAL_PAGES%:');
    $pagenavi_options['current_text'] = '%PAGE_NUMBER%';
    $pagenavi_options['page_text'] = '%PAGE_NUMBER%';
    $pagenavi_options['first_text'] = ('Primera');
    $pagenavi_options['last_text'] = ('Última');
    $pagenavi_options['next_text'] = 'Siguiente &raquo;';
    $pagenavi_options['prev_text'] = '&laquo; Anterior';
    $pagenavi_options['dotright_text'] = '...';
    $pagenavi_options['dotleft_text'] = '...';
    $pagenavi_options['num_pages'] = 5; //continuous block of page numbers
    $pagenavi_options['always_show'] = 0;
    $pagenavi_options['num_larger_page_numbers'] = 0;
    $pagenavi_options['larger_page_numbers_multiple'] = 5;

    //If NOT a single Post is being displayed
    /*http://codex.wordpress.org/Function_Reference/is_single)*/
    if (!is_single()) {
        $request = $wp_query->request;
        //intval — Get the integer value of a variable
        /*http://php.net/manual/en/function.intval.php*/
        $posts_per_page = intval(get_query_var('posts_per_page'));
        //Retrieve variable in the WP_Query class.
        /*http://codex.wordpress.org/Function_Reference/get_query_var*/
        $paged = intval(get_query_var('paged'));
        $numposts = $wp_query->found_posts;
        $max_page = $wp_query->max_num_pages;

        //empty — Determine whether a variable is empty
        /*http://php.net/manual/en/function.empty.php*/
        if(empty($paged) || $paged == 0) {
            $paged = 1;
        }

        $pages_to_show = intval($pagenavi_options['num_pages']);
        $larger_page_to_show = intval($pagenavi_options['num_larger_page_numbers']);
        $larger_page_multiple = intval($pagenavi_options['larger_page_numbers_multiple']);
        $pages_to_show_minus_1 = $pages_to_show - 1;
        $half_page_start = floor($pages_to_show_minus_1/2);
        //ceil — Round fractions up (http://us2.php.net/manual/en/function.ceil.php)
        $half_page_end = ceil($pages_to_show_minus_1/2);
        $start_page = $paged - $half_page_start;

        if($start_page <= 0) {
            $start_page = 1;
        }

        $end_page = $paged + $half_page_end;
        if(($end_page - $start_page) != $pages_to_show_minus_1) {
            $end_page = $start_page + $pages_to_show_minus_1;
        }
        if($end_page > $max_page) {
            $start_page = $max_page - $pages_to_show_minus_1;
            $end_page = $max_page;
        }
        if($start_page <= 0) {
            $start_page = 1;
        }

        $larger_per_page = $larger_page_to_show*$larger_page_multiple;
        //round_num() custom function - Rounds To The Nearest Value.
        $larger_start_page_start = (round_num($start_page, 10) + $larger_page_multiple) - $larger_per_page;
        $larger_start_page_end = round_num($start_page, 10) + $larger_page_multiple;
        $larger_end_page_start = round_num($end_page, 10) + $larger_page_multiple;
        $larger_end_page_end = round_num($end_page, 10) + ($larger_per_page);

        if($larger_start_page_end - $larger_page_multiple == $start_page) {
            $larger_start_page_start = $larger_start_page_start - $larger_page_multiple;
            $larger_start_page_end = $larger_start_page_end - $larger_page_multiple;
        }
        if($larger_start_page_start <= 0) {
            $larger_start_page_start = $larger_page_multiple;
        }
        if($larger_start_page_end > $max_page) {
            $larger_start_page_end = $max_page;
        }
        if($larger_end_page_end > $max_page) {
            $larger_end_page_end = $max_page;
        }
        if($max_page > 1 || intval($pagenavi_options['always_show']) == 1) {
            /*http://php.net/manual/en/function.str-replace.php */
            /*number_format_i18n(): Converts integer number to format based on locale (wp-includes/functions.php*/
            $pages_text = str_replace("%CURRENT_PAGE%", number_format_i18n($paged), $pagenavi_options['pages_text']);
            $pages_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pages_text);
            echo $before.'<div class="pagenavi">'."\n";

            if(!empty($pages_text)) {
                echo '<span class="pages">'.$pages_text.'</span>';
            }
            //Displays a link to the previous post which exists in chronological order from the current post.
            /*http://codex.wordpress.org/Function_Reference/previous_post_link*/
            previous_posts_link($pagenavi_options['prev_text']);

            if ($start_page >= 2 && $pages_to_show < $max_page) {
                $first_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['first_text']);
                //esc_url(): Encodes < > & " ' (less than, greater than, ampersand, double quote, single quote).
                /*http://codex.wordpress.org/Data_Validation*/
                //get_pagenum_link():(wp-includes/link-template.php)-Retrieve get links for page numbers.
                echo '<a href="'.esc_url(get_pagenum_link()).'" class="first" title="'.$first_page_text.'">1</a>';
                if(!empty($pagenavi_options['dotleft_text'])) {
                    echo '<span class="expand">'.$pagenavi_options['dotleft_text'].'</span>';
                }
            }

            if($larger_page_to_show > 0 && $larger_start_page_start > 0 && $larger_start_page_end <= $max_page) {
                for($i = $larger_start_page_start; $i < $larger_start_page_end; $i+=$larger_page_multiple) {
                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
                    echo '<a href="'.esc_url(get_pagenum_link($i)).'" class="single_page" title="'.$page_text.'">'.$page_text.'</a>';
                }
            }

            for($i = $start_page; $i  <= $end_page; $i++) {
                if($i == $paged) {
                    $current_page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['current_text']);
                    echo '<span class="current">'.$current_page_text.'</span>';
                } else {
                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
                    echo '<a href="'.esc_url(get_pagenum_link($i)).'" class="single_page" title="'.$page_text.'">'.$page_text.'</a>';
                }
            }

            if ($end_page < $max_page) {
                if(!empty($pagenavi_options['dotright_text'])) {
                    echo '<span class="expand">'.$pagenavi_options['dotright_text'].'</span>';
                }
                $last_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['last_text']);
                echo '<a href="'.esc_url(get_pagenum_link($max_page)).'" class="last" title="'.$last_page_text.'">'.$max_page.'</a>';
            }
            next_posts_link($pagenavi_options['next_text'], $max_page);

            if($larger_page_to_show > 0 && $larger_end_page_start < $max_page) {
                for($i = $larger_end_page_start; $i <= $larger_end_page_end; $i+=$larger_page_multiple) {
                    $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
                    echo '<a href="'.esc_url(get_pagenum_link($i)).'" class="single_page" title="'.$page_text.'">'.$page_text.'</a>';
                }
            }
            echo '</div>'.$after."\n";
        }
    }
}
/*
 |
 | Menus
 |
 */
register_nav_menu( 'Default', __( 'Default', 'ICT' ) );
register_nav_menu( 'Footer1', __( 'Footer1', 'ICT' ) );
register_nav_menu( 'Footer2', __( 'Footer2', 'ICT' ) );
/*
 |
 |
 |
 */

$args = array(
    'default-image' => get_template_directory_uri() . '/img/home/banner.jpg',
    'uploads'       => true,
);
add_theme_support( 'custom-header', $args );


