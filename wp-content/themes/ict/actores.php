<?php
/*
    Template Name: Página Actores
 */
wp_enqueue_style( 'index',  get_template_directory_uri() . '/index.css' );
get_header();
?>
    <div id="index">
        <div>
            <div id="banner-index">
                <img src="<?php bloginfo('template_url'); ?>/img/actores/banner.jpg " alt="<?php the_title() ?>"/>
            </div>
        </div>

    </div>

    <div  id="page-container">
        <div id="page-content">
            <div id="title-container">
                <div class="icon-title">
                    <img width="60px" src="<?php bloginfo('template_url'); ?>/img/actores/icono.png" alt="<?php the_title() ?>"/>
                </div>
                <div class="title-page">
                    <h1 style="font-size: 24px;margin-left: 10px;">
                        <?php the_title(); ?>
                    </h1>
                </div>
                <div class="clear"></div>
            </div>
            <div id="content-category">
                <?php
                if(have_posts()) {
                    while (have_posts()) {
                        the_post();
                        the_content();
                    }
                }
                ?>
                <div>
                    <h2><?php pll_e('Listado de Actores'); ?>:</h2>
                    <ul class="list-actors">
                    <?php
                    $args = array(

                        'post_type'        => 'actors'
                    );
                    $actor = get_posts( $args );

                    $args=array(
                        'post_type' => 'actors',
                        'order'     => 'ASC',
                        'order_by'  => 'menu_order',
                        'post_parent' => 0
                    );
                    $news = new WP_Query($args);
                    if($news->have_posts()) {
                        while ($news->have_posts()) {
                            $news->the_post();
                    ?>
                        <li >
                            <div class="logo-actors">
                                <div class="logo-actors-container">
                                <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                }
                                ?>
                                </div>
                            </div>
                            <div class="content-list-actors">
                                <div class="content-list-container">
                                    <h4>
                                        <a  style="color:#0F79A0" title="<?php the_title();?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h4>
                                    <?php
                                    the_excerpt();
                                    ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </li>
                    <?php
                        }
                    }
                    ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>


<?php
get_footer();
?>