<?php
/*
    Template Name: Contactenos Template

*/
wp_enqueue_style( 'index',  get_template_directory_uri() . '/index.css' );
get_header();
?>
    <div id="index">
        <div>
            <div id="banner-index">
                <img src="<?php bloginfo('template_url'); ?>/img//default/banner.jpg " alt="<?php the_title() ?>"/>
            </div>
        </div>

    </div>


    <div  id="page-container">
        <div id="page-content">
            <div id="title-container" class="background-claro">
                <div class="icon-title">
                    <img  class="icono-contactenos" src="<?php bloginfo('template_url'); ?>/img/contactenos/icono.png" alt="<?php the_title() ?>"/>
                </div>
                <div class="title-page">
                    <h1 style="font-size: 24px;margin-left: 10px;">
                        <?php  echo get_the_title($postParent->ID); ?>
                    </h1>
                </div>
            </div>

            <div id="content-page">
                <div class="col-2 divisor" >
                    <div class="logo-contactenos">
                        <img width="150" src="<?php bloginfo('template_url') ?>/img/contactenos/logo.png" alt="ICT GUATEMALA"/>
                    </div>

                    <div class="dirrecion">
                        <h2><?php pll_e('Dirección'); ?></h2>
                        <ul class="dirrecion-ul">
                            <li>PBX (502) 2423-8000</li>
                            <li>7 ave. Calle Dr. Eduardo Suger zona 10, Guatemala, Guatemala </li>
                            <li>Código postal 01010</li>
                        </ul>
                    </div>
                    <hr/>
                    <div  class="telefono-web">
                        <h2>Teléfono y web</h2>
                        <div>
                            <p>
                                PBX: (502) 2423 - 800
                            </p>
                            <p>
                                www.ictguatemala.edu
                            </p>
                        </div>
                    </div>
                    <hr/>
                </div>
                <div class="col-2 formulario-contacto">
                    <h2>Formulario</h2>
                    <?php
                    if(have_posts()) {
                        while (have_posts()) {
                            the_post();
                            the_content();
                        }
                    }
                    ?>
                </div>

                
                <div class="clear"></div>

            </div>
    </div>
</div>
<?php
get_footer();
?>