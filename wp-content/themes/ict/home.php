<?php
get_header();
?>
<div id="home">
    <div id="home-container">
        <div id="banner-home">
            <img src="<?php header_image(); ?>" alt="Home Proyecto ICT"/>
        </div>
        <div id="menu-banner">
            <ul>
                <li class="blue">
                    <?php
                    $args=array(
                        'post_type' => 'page',
                        'name'      => 'Actores'
                    );
                    $welcome = new WP_Query($args);
                    if($welcome->have_posts()) {
                        while ($welcome->have_posts()) {
                            $welcome->the_post();
                            ?>
                            <a href="<?php the_permalink() ?>" title="<?php the_title()?>">
                                <div>
                                    <div class="icon-home">
                                        <img src="<?php bloginfo('template_url')?>/img/home/actores.png" alt="<?php the_title()?>"/>
                                    </div>
                                        <h2><?php  the_title()?></h2>

                                    <div>
                                        <?php echo get_excerpt_content(get_the_excerpt(),80) ?>
                                    </div>
                                </div>

                            </a>
                        <?php
                        }
                    }
                    ?>

                </li>
                <li class="blue-dark">
                    <?php
                    $args=array(
                        'post_type' => 'page',
                        'name'      => 'por-que-guatemala'
                    );
                    $welcome = new WP_Query($args);
                    if($welcome->have_posts()) {
                        while ($welcome->have_posts()) {
                            $welcome->the_post();
                            ?>
                            <a href="<?php the_permalink() ?>" title="<?php the_title()?>">
                                <div>
                                    <div class="icon-home">
                                        <img src="<?php bloginfo('template_url')?>/img/home/porque_guatemala.png" alt="<?php the_title()?>"/>
                                    </div>
                                    <h2><?php  the_title()?></h2>
                                    <div>
                                        <?php echo get_excerpt_content(get_the_excerpt(),80) ?>
                                    </div>
                                </div>

                            </a>
                        <?php
                        }
                    }
                    ?>
                </li>
                <li class="purpe">
                    <?php
                    $args=array(
                        'post_type' => 'page',
                        'name'      => 'idealist'
                    );
                    $welcome = new WP_Query($args);
                    if($welcome->have_posts()) {
                        while ($welcome->have_posts()) {
                            $welcome->the_post();
                            ?>
                            <a href="<?php the_permalink() ?>" title="<?php the_title()?>">
                                <div>
                                    <div class="icon-home">
                                        <img src="<?php bloginfo('template_url')?>/img/home/idealist.png" alt="<?php the_title()?>"/>
                                    </div>
                                    <h2><?php  the_title()?></h2>
                                    <div>
                                        <?php echo get_excerpt_content(get_the_excerpt(),80) ?>
                                    </div>
                                </div>

                            </a>
                        <?php
                        }
                    }
                    ?>
                </li>

            </ul>
        </div>
    </div>
</div>
<div id="welcome">
    <div>
        <?php
        $args=array(
            'post_type' => 'page',
            'name'      => 'Bienvenidos'
        );
        $welcome = new WP_Query($args);
        if($welcome->have_posts()){
            while($welcome->have_posts()){
                $welcome->the_post();
                ?>
                <h1><?php the_title(); ?></h1>
                <div class="text-welcome">
                    <?php the_content() ?>
                </div>
            <?php
            }
        }
        ?>
    </div>
</div>

<?php
/*
 * Sección Carucel de Convocatorias
 */
$args=array(
    'post_type'     => 'post',
    'category_name' => 'convocatorias'
);
$calls = new WP_Query($args);
if($calls->have_posts()){
    ?>
    <div style="background: #E7E7E7; padding: 30px; height: 425px;">
        <h1 style="margin: 0px">
            <?php
                $category = get_category_by_slug( 'convocatorias' );
                $title = get_the_category_by_id(pll_get_term($category->term_id));
                echo $title;
            ?>
        </h1>
        <div id="convocatorias-container">
            <?php
            while($calls->have_posts()){
                $calls->the_post();
                ?>
                <div class="convocatoria">
                    <div class="header-convacatoria">
                        <div class="fecha-convocatoria">
                            <div>
                                <div class="dia">
                                    <?php
                                       $dia = get_post_meta(get_the_ID(),'dia-convocatoria',true);
                                       echo $dia;
                                    ?>
                                </div>
                                <div class="mes">
                                    <?php
                                        $mes = get_post_meta(get_the_ID(),'mes-convocatoria',true);
                                        echo $mes;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="titulo-convocatoria">
                            <div>
                                <?php the_title('<h3>','</h3>')?>
                            </div>
                        </div>
                    </div>
                    <div class="content-convocatoria">
                        <div class="post-content">
                            <?php
                            the_excerpt();
                            ?>
                        </div>
                        <div>
                            <a href="<?php the_permalink() ?>" title="Titulo" class="ver-mas"><?php pll_e('ver más'); ?>..</a>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
<?php
}
wp_reset_query();
/*
 * Sección Noticias
 */
$args=array(
    'post_type'     => 'post',
    'category_name' => 'noticias',
    'posts_per_page'=> 3
);
$news = new WP_Query($args);
if($news->have_posts()) {
    ?>
    <div style="background: #D9D9D9; padding: 30px; height:400px;">
        <h1 style="margin: 0px">
            <?php
            $category = get_category_by_slug( 'noticias' );
            echo get_the_category_by_id(pll_get_term($category->term_id));
            ?>
        </h1>

        <div id="noticias-container">
            <?php
            while ($news->have_posts()) {
                $news->the_post();
                ?>
                <div class="convocatoria">
                    <div class="header-convacatoria">
                        <?php the_title('<h3>','</h3>') ?>
                    </div>
                    <div class="content-noticia">
                        <div class="post-content">
                            <?php the_excerpt() ?>
                        </div>
                        <div>
                            <a href="<?php the_permalink() ?>" title="Titulo" class="ver-mas"><?php pll_e('ver más'); ?>..</a>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <div id="mas-noticias">
        <a href="<?php  echo get_category_link($category->term_id) ?>">Más noticias<i class="fa fa-angle-right" style="margin-left: 5px"></i></a>
    </div>
<?php
}
wp_reset_query();
_e('','ICT');

?>

<?php
get_footer();
?>
