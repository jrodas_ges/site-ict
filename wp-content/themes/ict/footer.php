<div class="footer">
    <div>
        <?php
        wp_nav_menu( array( 'theme_location' => 'Footer1' ) );
        ?>
    </div>

    <div>
        <?php
            wp_nav_menu( array( 'theme_location' => 'Footer2' ) );
        ?>
    </div>

    <div>
        <ul>
            <?php
            $args=array(
                'post_type'   => 'page',
                'order'       => 'ASC',
                'order_by'    => 'menu_order',
                'name'        => 'idealist',
                'numberposts' => 1,
                'post_status' => 'publish',
            );
            $pageMain = new WP_Query($args);
            if($pageMain->have_posts()) {
                while ($pageMain->have_posts()) {
                    $pageMain->the_post();
                    ?>
                    <li class="sub-list-fater"><?php the_title()?></li>
                    <li>
                        <ul class="sub-list">
                            <?php
                            $args = array(
                                'post_type' => 'page',
                                'order' => 'ASC',
                                'order_by' => 'menu_order',
                                'post_parent' => get_the_ID(),
                                'post_status' => 'publish',
                            );
                            $page = new WP_Query($args);
                            if($page->have_posts()) {
                                while ($page->have_posts()) {
                                    $page->the_post();
                                    ?>
                                    <li><a href="<?php  the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
                                <?php
                                }
                            }
                            wp_reset_query();
                            ?>
                        </ul>
                    </li>
                <?php
                }
            }
            wp_reset_query();
            ?>
        </ul>
    </div>

    <div>
        <ul>
            <?php
            $args=array(
                'post_type'   => 'page',
                'order'       => 'ASC',
                'order_by'    => 'menu_order',
                'name'        => 'por-que-guatemala',
                'numberposts' => 1,
                'post_status' => 'publish',
            );
            $pageMain = new WP_Query($args);
            if($pageMain->have_posts()) {
                while ($pageMain->have_posts()) {
                    $pageMain->the_post();
                ?>
                <li class="sub-list-fater">
                    <a href="<?php the_permalink();?>" title="<?php the_title(); ?>">
                        <?php the_title()?>
                    </a>
                </li>
                <li>
                    <ul class="sub-list">
                        <?php
                        $args = array(
                            'post_type' => 'page',
                            'order' => 'ASC',
                            'order_by' => 'menu_order',
                            'post_parent' => get_the_ID(),
                            'post_status' => 'publish',
                        );
                        $page = new WP_Query($args);
                        if($page->have_posts()) {
                            while ($page->have_posts()) {
                                $page->the_post();
                                ?>
                                <li><a href="<?php  the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
                            <?php
                            }
                        }
                        wp_reset_query();
                        ?>
                    </ul>
                </li>
            <?php
                }
            }
            wp_reset_query();
            ?>
        </ul>
    </div>

    <div>
        <ul>
            <li class="sub-list-fater"><?php pll_e('contáctenos'); ?></li>
            <li class="footer-list-final">
                <ul class="sub-list">
                    <li>PBX (502) 2423-8000</li>
                    <li>7 ave.<?php pll_e('Calle'); ?> Dr. Eduardo Suger <?php pll_e('zona'); ?> 10, Guatemala, Guatemala </li>
                    <li><?php pll_e('Código postal'); ?> 01010</li>
                </ul>
            </li>
        </ul>
    </div>


</div>
<div class="copy">
    Todos los derechos recervados Guatemala C.A  &copy; 2015 power by: Departamento GES Universidad Galileo
</div>
</div>
<div>

</div>
<script>
    $('#convocatorias-container').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3
    });
    $('.slick-prev').html('');
    $('.slick-next').html('');
</script>
<script src="js/main.js"></script>
<?php wp_footer(); ?>
</body>
</html>