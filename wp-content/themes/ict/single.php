<?php
wp_enqueue_style( 'index',  get_template_directory_uri() . '/index.css' );
get_header();
if(have_posts()) {
    while (have_posts()) {
?>
    <div id="index" style="background: #666">
        <div>
            <div id="banner-index">
                <?php
                    $category = get_the_category();
                    $categoryName =$category[0]->name;
                    include (get_template_directory() . '/path-category.php');
                    echo '<img src="' . get_bloginfo('template_url') . '/img/'. $path . '/banner.jpg" alt=""/>';
                ?>
                
            </div>
        </div>

    </div>

    <div  id="page-container">
        <div id="page-content">
            <div id="title-container" class="background-claro">
                <div class="icon-title">
                    <div>
                        <img  class="icono-<?php echo $path ?>" src="<?php bloginfo('template_url'); ?>/img/<?php echo $path ?>/icono.png" alt="<?php the_title() ?>"/>
                    </div>
                </div>
                <div class="title-page">
                    <h1>
                        <?php
                              echo $categoryName;
                        ?>
                    </h1>
                </div>

            </div>
            <div id="content-category">
                <?php

                        the_post();
                        ?>
                        <h2><?php the_title() ?></h2>
                        <?php the_content() ?>

            </div>
        </div>
    </div>

    <?php
    }
}
?>
<?php
get_footer();
?>