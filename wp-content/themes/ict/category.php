<?php
wp_enqueue_style( 'index',  get_template_directory_uri() . '/index.css' );
get_header();
$category = get_the_category();
$categoryName =$category[0]->name;

$category = get_the_category();
$categoryName =$category[0]->name;
include (get_template_directory() . '/path-category.php');
?>
    <div id="index">
        <div>
            <div id="banner-index">
                <?php
                    echo '<img src="' . get_bloginfo('template_url') . '/img/'. $path . '/banner.jpg" alt="' . $category[0]->name .'"/>';
                ?>
            </div>
        </div>

    </div>

    <div  id="page-container">
        <div id="page-content">
                       <div id="title-container" class="background-claro">
                <div class="icon-title">
                    <img  class="icono-<?php echo $path ?>" src="<?php bloginfo('template_url'); ?>/img/<?php echo $path ?>/icono.png" alt="<?php the_title() ?>"/>
                </div>
                <div class="title-page">
                    <h1>
                        <?php
                            echo $categoryName;
                        ?>
                    </h1>
                </div>
       </div>
            <div id="index-content">

                <?php
                if(have_posts()) {
                    while (have_posts()) {
                        echo '<div class="list-post-category">';
                        the_post();
                ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" ><?php the_title('<h1>','</h2>');?></a>
                <?php
                        echo '<a href="' . get_the_permalink() . '" title="' . get_the_title() .'">' . get_the_title('<h2>','</h2>')  . '</a>';
                        the_excerpt();
                        echo'<div style="float: right"><a class="ver-mas-category" href="' . get_the_permalink() . '"> Ver mas </a></div>  ';
                        echo '</div>';
                    }
                }
                ?>
                <div class="clear"></div>
                <div class="navigation"><?php if(function_exists('pagenavi')) { pagenavi(); } ?></div>
            </div>
        </div>
   </div>


<?php
get_footer();
?>